package com.p3dev.schoolbadges;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolbadgesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolbadgesApplication.class, args);
	}

}

package com.p3dev.schoolbadges.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.p3dev.schoolbadges.entities.Badge;

@RepositoryRestResource
public interface BadgesRepository extends JpaRepository<Badge, Long>{}
